#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <WinApi.au3>
#include <Misc.au3>

#include <GUICtrlOnHover.au3>
#include <WinApiEx.au3>

Global $onHoverShowItems = True
Global $TabCurSel = 1
$WinVers = Number(_WinAPI_GetVersion())
$Form1 = GUICreate("Tabs", 615, 437, 192, 124)
	GUISetBkColor(4079166)

$TabItem1 = GUICtrlCreateLabel("", 152, 24, 30, 57)
	GUICtrlSetBkColor($TabItem1, 0x1A1A1A)
	GUICtrlSetCursor($TabItem1,0)

$TabItem2 = GUICtrlCreateLabel("", 152, 80, 30, 57)
	GUICtrlSetBkColor($TabItem2, 0x4F4F4F)
	GUICtrlSetCursor($TabItem2,0)

$TabItem3 = GUICtrlCreateLabel("", 152, 136, 30, 57)
	GUICtrlSetBkColor($TabItem3, 0x4F4F4F)
	GUICtrlSetCursor($TabItem3,0)

$TabBk = GUICtrlCreateLabel("", 8, 24, 148, 409)
	GUICtrlSetBkColor($TabBk, 0x1A1A1A)
	GUICtrlSetState($TabBk,$GUI_DISABLE)
	GUICtrlSetState($TabBk,$TabBk)

$ProjectTreeView = GUICtrlCreateTreeView(12,28,140,400)
	$generalitem = GUICtrlCreateTreeViewItem("General", $ProjectTreeView)
    $displayitem = GUICtrlCreateTreeViewItem("Display", $ProjectTreeView)
    $aboutitem = GUICtrlCreateTreeViewItem("About", $generalitem)
    $compitem = GUICtrlCreateTreeViewItem("Computer", $generalitem)
    GUICtrlCreateTreeViewItem("User", $generalitem)
    GUICtrlCreateTreeViewItem("Resolution", $displayitem)
    GUICtrlCreateTreeViewItem("Other", $displayitem)
	If $WinVers > 6.0 Then _WinAPI_SetWindowTheme(GUICtrlGetHandle($ProjectTreeView), 'Explorer')

$ObjectInspectorListView = GUICtrlCreateListView('Column 1|Column 2',12,28,140,400)
	For $i = 1 To 9
		GUICtrlCreateListViewItem('Item ' & $i & '|' & 'Sub ' & $i , $ObjectInspectorListView)
	Next
	GUICtrlSetState($ObjectInspectorListView,$GUI_HIDE)
	If $WinVers > 6.0 Then _WinAPI_SetWindowTheme(GUICtrlGetHandle($ObjectInspectorListView), 'Explorer')

$TabItem3ListView = GUICtrlCreateListView("Item1",12,28,140,400,0x4000)
	GUICtrlSetState($TabItem3ListView,$GUI_HIDE)
	For $i = 1 To 9
		GUICtrlCreateListViewItem('Item ' & $i, $TabItem3ListView)
	Next
	If $WinVers > 6.0 Then _WinAPI_SetWindowTheme(GUICtrlGetHandle($TabItem3ListView), 'Explorer')
GUISetState(@SW_SHOW)

$CheckBoxShowInsideTabs = GUICtrlCreateCheckbox("Show items inside Tabs on Hover",200,200,300,17)
GUICtrlSetState($CheckBoxShowInsideTabs,$GUI_CHECKED)
Global $ShowTip, $CtrlIDPos
_GUiCtrlsetOnHover($TabItem1,"_OnHover","_OnNormal","_OnClick","",0)
_GUiCtrlsetOnHover($TabItem2,"_OnHover","_OnNormal","_OnClick","",0)
_GUiCtrlsetOnHover($TabItem3,"_OnHover","_OnNormal","_OnClick","",0)

GUICtrlSetState($ProjectTreeView,$GUI_SHOW)
While 1
	$nMsg = GUIGetMsg()
	Switch $nMsg
		Case $GUI_EVENT_CLOSE
			Exit
		Case $CheckBoxShowInsideTabs
			If BitAND(GUICtrlRead($CheckBoxShowInsideTabs),$GUI_CHECKED) = $GUI_CHECKED Then
				$onHoverShowItems = True
			Else
				$onHoverShowItems = False
			EndIf
	EndSwitch
WEnd

Func _OnHover($CtrlID)
	If $ShowTip <> 0 Then
		AdlibUnRegister("_OnNormalTip")
		For $i = 160 To 0 step -10
			GUICtrlSetPos($ShowTip,$CtrlIDPos[0]+$CtrlIDPos[2],($CtrlIDPos[1]+$CtrlIDPos[3])-($CtrlIDPos[3]/2)-20,$i)
		Next
		$i = 160
		GUICtrlDelete($ShowTip)
		$ShowTip = 0
		Switch $TabCurSel
			Case 1
				GUICtrlSetState($ProjectTreeView,$GUI_SHOW)
				GUICtrlSetState($ObjectInspectorListView,$GUI_HIDE)
				GUICtrlSetState($TabItem3ListView,$GUI_HIDE)
				GUICtrlSetBkColor($TabItem1, 0x1A1A1A)
				GUICtrlSetBkColor($TabItem2, 0x4F4F4F)
				GUICtrlSetBkColor($TabItem3, 0x4F4F4F)
			Case 2
				GUICtrlSetState($ProjectTreeView,$GUI_HIDE)
				GUICtrlSetState($ObjectInspectorListView,$GUI_SHOW)
				GUICtrlSetState($TabItem3ListView,$GUI_HIDE)
				GUICtrlSetBkColor($TabItem1, 0x4F4F4F)
				GUICtrlSetBkColor($TabItem2, 0x1A1A1A)
				GUICtrlSetBkColor($TabItem3, 0x4F4F4F)
			Case 3
				GUICtrlSetState($ProjectTreeView,$GUI_HIDE)
				GUICtrlSetState($ObjectInspectorListView,$GUI_HIDE)
				GUICtrlSetState($TabItem3ListView,$GUI_SHOW)
				GUICtrlSetBkColor($TabItem1, 0x4F4F4F)
				GUICtrlSetBkColor($TabItem2, 0x4F4F4F)
				GUICtrlSetBkColor($TabItem3, 0x1A1A1A)
		EndSwitch
	EndIf
	$CtrlIDPos = ControlGetPos($Form1,"",$CtrlID)
	Local $tText = ""
	Switch $CtrlID
		Case $TabItem1
			$tText = "TabItem1"
		Case $TabItem2
			$tText = "TabItem2"
		Case $TabItem3
			$tText = "TabItem3"
	EndSwitch
	$ShowTip = GUICtrlCreateLabel($tText,$CtrlIDPos[0]+$CtrlIDPos[2],($CtrlIDPos[1]+$CtrlIDPos[3])-($CtrlIDPos[3]/2)-20,1,40,$SS_CENTERIMAGE)
		GUICtrlSetBkColor($ShowTip,0x333333)
		GUICtrlSetColor($ShowTip,0x8A8A8A)
		GUICtrlSetFont($ShowTip,12,400,0,"Consolas")
	GUICtrlSetBkColor($TabBk,0x333333)
	Switch GUICtrlRead($ShowTip)
		Case "TabItem1"
			If $onHoverShowItems = True Then
				GUICtrlSetState($ProjectTreeView,$GUI_SHOW)
				GUICtrlSetState($ObjectInspectorListView,$GUI_HIDE)
				GUICtrlSetState($TabItem3ListView,$GUI_HIDE)
			EndIf
			GUICtrlSetBkColor($TabItem1, 0x333333)
			If $TabCurSel = 2 Then
				GUICtrlSetBkColor($TabItem2, 0x1A1A1A)
				GUICtrlSetBkColor($TabItem3, 0x4F4F4F)
			ElseIf $TabCurSel = 3 Then
				GUICtrlSetBkColor($TabItem2, 0x4F4F4F)
				GUICtrlSetBkColor($TabItem3, 0x1A1A1A)
			EndIf
		Case "TabItem2"
			If $onHoverShowItems = True Then
				GUICtrlSetState($ProjectTreeView,$GUI_HIDE)
				GUICtrlSetState($ObjectInspectorListView,$GUI_SHOW)
				GUICtrlSetState($TabItem3ListView,$GUI_HIDE)
			EndIf
			GUICtrlSetBkColor($TabItem2, 0x333333)
			If $TabCurSel = 1 Then
				GUICtrlSetBkColor($TabItem1, 0x1A1A1A)
				GUICtrlSetBkColor($TabItem3, 0x4F4F4F)
			ElseIf $TabCurSel = 3 Then
				GUICtrlSetBkColor($TabItem1, 0x4F4F4F)
				GUICtrlSetBkColor($TabItem3, 0x1A1A1A)
			EndIf
		Case "TabItem3"
			If $onHoverShowItems = True Then
				GUICtrlSetState($ProjectTreeView,$GUI_HIDE)
				GUICtrlSetState($ObjectInspectorListView,$GUI_HIDE)
				GUICtrlSetState($TabItem3ListView,$GUI_SHOW)
			EndIf
			GUICtrlSetBkColor($TabItem3, 0x333333)
			If $TabCurSel = 1 Then
				GUICtrlSetBkColor($TabItem1, 0x1A1A1A)
				GUICtrlSetBkColor($TabItem2, 0x4F4F4F)
			ElseIf $TabCurSel = 2 Then
				GUICtrlSetBkColor($TabItem1, 0x4F4F4F)
				GUICtrlSetBkColor($TabItem2, 0x1A1A1A)
			EndIf
	EndSwitch
	AdlibRegister("_OnHoverTip",10)
EndFunc

Func _OnHoverTip()
	Local Static $i
	GUICtrlSetPos($ShowTip,$CtrlIDPos[0]+$CtrlIDPos[2],($CtrlIDPos[1]+$CtrlIDPos[3])-($CtrlIDPos[3]/2)-20,$i)
	$i += 10
	If $i >= 160 Then
		$i = 0
		AdlibUnRegister("_OnHoverTip")
	EndIf
EndFunc

Func _OnNormal($CtrlID)
	GUICtrlSetBkColor($TabBk,0x1A1A1A)

	AdlibRegister("_OnNormalTip",10)
EndFunc

Func _OnNormalTip()
	Local $mMsg = GUIGetCursorInfo($Form1)
	Local Static $i = 160
	GUICtrlSetPos($ShowTip,$CtrlIDPos[0]+$CtrlIDPos[2],($CtrlIDPos[1]+$CtrlIDPos[3])-($CtrlIDPos[3]/2)-20,$i)
	$i -= 10
	If $i <= 0 Then
		$i = 160
		GUICtrlDelete($ShowTip)
		$ShowTip = 0
		Switch $TabCurSel
			Case 1
				GUICtrlSetState($ProjectTreeView,$GUI_SHOW)
				GUICtrlSetState($ObjectInspectorListView,$GUI_HIDE)
				GUICtrlSetState($TabItem3ListView,$GUI_HIDE)
				GUICtrlSetBkColor($TabItem1, 0x1A1A1A)
				GUICtrlSetBkColor($TabItem2, 0x4F4F4F)
				GUICtrlSetBkColor($TabItem3, 0x4F4F4F)
			Case 2
				GUICtrlSetState($ProjectTreeView,$GUI_HIDE)
				GUICtrlSetState($ObjectInspectorListView,$GUI_SHOW)
				GUICtrlSetState($TabItem3ListView,$GUI_HIDE)
				GUICtrlSetBkColor($TabItem1, 0x4F4F4F)
				GUICtrlSetBkColor($TabItem2, 0x1A1A1A)
				GUICtrlSetBkColor($TabItem3, 0x4F4F4F)
			Case 3
				GUICtrlSetState($ProjectTreeView,$GUI_HIDE)
				GUICtrlSetState($ObjectInspectorListView,$GUI_HIDE)
				GUICtrlSetState($TabItem3ListView,$GUI_SHOW)
				GUICtrlSetBkColor($TabItem1, 0x4F4F4F)
				GUICtrlSetBkColor($TabItem2, 0x4F4F4F)
				GUICtrlSetBkColor($TabItem3, 0x1A1A1A)
		EndSwitch
		AdlibUnRegister("_OnNormalTip")
	EndIf
EndFunc

Func _OnClick($CtrlId)

	Switch GUICtrlRead($ShowTip)
		Case "TabItem1"
			GUICtrlSetState($ProjectTreeView,$GUI_SHOW)
			GUICtrlSetState($ObjectInspectorListView,$GUI_HIDE)
			GUICtrlSetState($TabItem3ListView,$GUI_HIDE)
			$TabCurSel = 1
		Case "TabItem2"
			GUICtrlSetState($ProjectTreeView,$GUI_HIDE)
			GUICtrlSetState($ObjectInspectorListView,$GUI_SHOW)
			GUICtrlSetState($TabItem3ListView,$GUI_HIDE)
			$TabCurSel = 2
		Case "TabItem3"
			GUICtrlSetState($ProjectTreeView,$GUI_HIDE)
			GUICtrlSetState($ObjectInspectorListView,$GUI_HIDE)
			GUICtrlSetState($TabItem3ListView,$GUI_SHOW)
			$TabCurSel = 3
	EndSwitch
	AdlibRegister("_OnClickTip",10)
EndFunc

Func _OnClickTip()
	AdlibUnRegister("_OnClickTip")
	GUICtrlSetBkColor($ShowTip,0x1A1A1A)
	GUICtrlSetBkColor($TabBk,0x1A1A1A)
	Switch $TabCurSel
		Case 1
			GUICtrlSetBkColor($TabItem1, 0x1A1A1A)
			GUICtrlSetBkColor($TabItem2, 0x4F4F4F)
			GUICtrlSetBkColor($TabItem3, 0x4F4F4F)
		Case 2
			GUICtrlSetBkColor($TabItem1, 0x4F4F4F)
			GUICtrlSetBkColor($TabItem2, 0x1A1A1A)
			GUICtrlSetBkColor($TabItem3, 0x4F4F4F)
		Case 3
			GUICtrlSetBkColor($TabItem1, 0x4F4F4F)
			GUICtrlSetBkColor($TabItem2, 0x4F4F4F)
			GUICtrlSetBkColor($TabItem3, 0x1A1A1A)
	EndSwitch
EndFunc